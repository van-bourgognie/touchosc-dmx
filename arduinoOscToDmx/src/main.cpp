#include <Ethernet.h>
#include <EthernetUdp.h>
#include <OSCBundle.h>
#include <ArduinoDMX.h>

#define DEBUG 0
#define TCP

/* Reset vector in case OSC runs into error condition */
void(* reset) (void) = 0;

/* DMX */
#define UNIV_SZ 512
uint8_t universe[UNIV_SZ];

/* Ethernet */
IPAddress    ip(10, 42, 0,  2);
// Set a different MAC address per instance of the device
byte mac[] = { 0x98, 0x76, 0xB6, 0x12, 0x0E, 0xEA };

#ifdef TCP
EthernetServer server = EthernetServer(5432);
#else
EthernetUDP Udp;
#endif

void setup() {

#if DEBUG
  /* DMX init */
  Serial.begin(9600);
  Serial.println("Started OSC to DMX");
#else
  DMX.begin(UNIV_SZ);
  DMX.writeAll(0);
#endif

  /* Ethernet init */
  Ethernet.init(10);
  Ethernet.begin(mac, ip);
#ifdef TCP
  server.begin();
#else
  Udp.begin(5432);
#endif
}

void setDmx(OSCMessage &msg, int addrOffset ) {
  int size = msg.size();

  // Error: DMX message too short, need address and at least 1 value
  if(size < 2)
    return;

  //ERROR: DMX first argument needs to be integer
  if(!msg.isInt(0))
    return;

  int offset = msg.getInt(0);
  //Start address too large
  if(offset > UNIV_SZ)
    return;

  // Error: DMX start address too small
  if(offset < 1)
    return;

  for(int i = 0; i < size - 1; i++) {
    // Error: DMX arguments need tot be int
    if(!msg.isInt(i + 1))
      continue;

    int value = msg.getInt(i + 1);

    // Error: Addressing beyond universe end
    if (offset + i > UNIV_SZ)
      break;
    universe[offset + i - 1] = value;
#if DEBUG
    Serial.print(offset+i);
    Serial.print(F(" > "));
    Serial.println(value);
#endif
  }
}

void updateDmx(OSCMessage &msg, int addrOffset ) {
#if DEBUG
  Serial.println("DMX update");
#else
  DMX.beginTransmission();
  for(int i = 0; i < UNIV_SZ; i++) {
    DMX.write(i + 1, universe[i]);
  }
  DMX.endTransmission();
#endif
}

union MsgLength{
    char bytes[4];
    uint32_t value;
};

MsgLength len;

void loop() {
  bool got_message = false;

#ifdef TCP
  OSCMessage msg;
  EthernetClient client = server.available();

  if (client) {
    if(client.connected()) {
      for(int i=0; i<4; i++) {
        while(!client.available());
        len.bytes[i] = client.read();
      }

      while (len.value--) {
        while(!client.available());
        msg.fill(client.read());
      }
      got_message = true;
    }
  }
#else //UDP
  OSCBundle msg;
  int size;

  if( (size = Udp.parsePacket()) > 0) {
    while(size--)
      msg.fill(Udp.read());
    got_message = true;
  }
#endif

  if (got_message) {
    if(msg.hasError()) {
      OSCErrorCode e = msg.getError();

      Serial.print(F("Error in message ["));
      Serial.print(e);

      switch(msg.getError()) {
        case ALLOCFAILED:
          Serial.println(F("] - Resetting..."));
          delay(1000);
          reset();
          break;
      }
    }
    msg.route("/dmx/set", setDmx);
    msg.route("/dmx/update", updateDmx);
  }
}