# iOS DMX light control

The goal of this project is to use an iPad to control DMX-controlled LED fixtures.

Two main components are:
1. TouchOSC on iPad (this transmits MIDI)
2. MIDI-to-DMX bridge: Arduino Leonardo as USB-MIDI device, translating MIDI to DMX

## Motivation

We need a lighting control console which is user friendly and reliable.

We use TouchOSC as it allows to easily build a UI and is built to transmit control signals. It can run on an iPad which can be locked into single app usage and more using "Guided Access".

TouchOSC can transmit MIDI to a USB-midi device, which can be connected to the iPad with the Camera Adapter accessory.

A USB-midi device can easily be made using the USBMIDI library on an Arduino Leonardo, allowing for all required workarounds imposed by the limitations of MIDI.

## Connecting

The MIDI-to-DMX device is an Arduino Leonardo running the MIDIUSB library. It connects to the iPad using the Camera Adapter. This allows for the iPad to charge while having an external device connected.

**_NOTE:_** When connecting the camera adapter, follow these steps
1. Connect a lightning power supply to the adapter
2. Connect the Arduino to the adapter
3. Connect the adapter to the ipad

If the adapter is connected to the iPad without having power supplied, the next time you supply power to it, the USB device will become disconnected and will not come back. I consider this an apple bug / shortcoming.

## MIDI message crafting

MIDI is very limited in its messages, allowing only 7 bits per byte to be used as data. We therefore split our bits over the status and data bytes on the iPad and reconstruct the DMX (or other) data.

Details in the source code for now.

## Screenshots and photos

Will follow

## Infrared control

To be documented

## Housing design and photos

Add freecad design, drawings and photos.