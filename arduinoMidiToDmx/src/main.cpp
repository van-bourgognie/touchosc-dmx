
#include <MIDIUSB.h>
#include <ArduinoDMX.h>

/* Reset vector in case OSC runs into error condition */
void(* reset) (void) = 0;

/* DMX */
#define UNIV_SZ 512

/* MIDI */
#define MIDI_NOTE_OFF       0x8
#define MIDI_NOTE_ON        0x9
#define MIDI_PITCHBEND      0xE

/* IR */
#ifdef WITH_IR
#define IR_SEND_PIN 10
#define IR_REPEAT             5
#define RAW_BUFFER_LENGTH 100
#include <IRremote.hpp>
IRsend irsend;
#endif

/* DEBUG */
// #define DEBUG
#ifdef DEBUG
#define DBG Serial
#endif

void setup() {
#ifdef DEBUG
  DBG.begin(1000000);
  DBG.println("Started USB-MIDI to DMX");
#endif
  DMX.begin(UNIV_SZ);
  DMX.beginTransmission();

#ifdef WITH_IR
  /* IR transmitter */
  pinMode(IR_SEND_PIN, OUTPUT);
  irsend.begin(); // Start with IR_SEND_PIN as send pin and enable feedback LED at default feedback LED pin
#endif
}

void sendIR(uint16_t sAddress, uint16_t sCommand, uint8_t sRepeats) {
#ifdef DEBUG
    DBG.println();
    DBG.print(F("Send NEC with 8 bit address"));
    DBG.print(F("[0x"));
    DBG.print(sAddress, HEX);
    DBG.print(F(":0x"));
    DBG.print(sCommand, HEX);
    DBG.print(F(":0x"));
    DBG.print(sRepeats, HEX);
    DBG.println(F("]"));
    DBG.flush();
#endif

#ifdef WITH_IR
    irsend.sendNEC(sAddress, sCommand, sRepeats);
#endif
}

void midiUnpackData(midiEventPacket_t rx, uint16_t* addr, uint8_t* value) {
  *addr  = (rx.byte1 << 5 & 0x0180) | (rx.byte2 & 0x007f);
  *value = (rx.byte1 << 7 & 0x0080) | (rx.byte3 & 0x007f);
}

void loop() {
  midiEventPacket_t rx;
  uint8_t   cmd;
  uint16_t addr;
  uint8_t value;

  do {
    rx = MidiUSB.read();
    if (rx.header != 0) {
      cmd = rx.byte1 >> 4 & 0xff;
      switch(cmd) {
      case MIDI_NOTE_ON:
        midiUnpackData(rx, &addr, &value);
        DMX.write(addr + 1, value);
        break;
      case MIDI_NOTE_OFF:
#ifdef DEBUG
        DBG.println("DMX transmit");
#endif
        DMX.endTransmission();
        DMX.beginTransmission();
        break;

#ifdef WITH_IR
      case MIDI_PITCHBEND:
        midiUnpackData(rx, &addr, &value);
        sendIR(addr & 0xff, value, IR_REPEAT);
        break;
#endif

      default:
#ifdef DEBUG
        DBG.print(F("Unhandled MIDI sequence: 0x"));
        DBG.print(rx.header, HEX);
        DBG.print(F("-"));
        DBG.print(rx.byte1, HEX);
        DBG.print(F("-"));
        DBG.print(rx.byte2, HEX);
        DBG.print(F("-"));
        DBG.print(rx.byte3, HEX);
        DBG.flush();
#endif
        break;
      }
    }
  } while (rx.header != 0);
}
