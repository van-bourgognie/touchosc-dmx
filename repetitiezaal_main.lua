--------------------------------------
--          HELPER FUNCTIONS        --
--------------------------------------

function indexOf(array, value)
  for i, v in ipairs(array) do
      if v == value then
          return i
      end
  end
  return nil
end

function dump(o)
 if type(o) == 'table' then
    local s = '{ '
    for k,v in pairs(o) do
       if type(k) ~= 'number' then k = '"'..k..'"' end
       s = s .. '['..k..'] = ' .. dump(v) .. ','
    end
    return s .. '} '
 else
    return tostring(o)
 end
end


-- Converts 2 words to MIDI Channel + 2 data bytes
--
-- address can be up to 9 bits long
--
-- value can be up to 8 bits long
--
function bytesToMidi(midi_message, addr, value)
    local value = math.floor(value)
    local channel = math.floor(addr / 128) * 4
    channel = channel + math.floor(value / 128)
    local data_1 = addr % 128
    local data_2 = value % 128
    return { midi_message + channel, data_1, data_2 }
end


--------------------------------------
--            COLOR MAPS            --
--------------------------------------

-- RGBAL maps
temperatures = {
{198, 065, 056, 220, 230}, -- 2800K
{181, 078, 074, 220, 230}, -- 3000K
{165, 092, 093, 220, 230}, -- 3200K
{153, 128, 111, 220, 230}, -- 3500K
{127, 128, 163, 220, 230}, -- 4000K
{106, 128, 200, 220, 230}, -- 4500K
{092, 128, 230, 220, 230}, -- 5000K
{073, 128, 255, 220, 230}, -- 5600K
{048, 128, 255, 220, 230}, -- 6000K
{019, 128, 255, 220, 230}, -- 6500K
}

colors = {
light_yellow  = {224, 158, 047, 255, 231},
yellow        = {180, 060, 000, 245, 255}, -- C3040 - Lt Yellow
light_amber   = {255, 000, 025, 255, 194}, -- C2040 - Lt Amber
amber         = {255, 000, 024, 255, 150}, -- C2050 - Md Amber
light_red     = {255, 000, 000, 070, 000}, -- C2060 - Dk Amber
red           = {255, 000, 000, 000, 000}, -- C1050 - Lt Red
light_magenta = {255, 050, 255, 000, 000}, -- C1060 - Dk Red Amber
magenta       = {255, 000, 255, 000, 000}, -- C1650 - Magenta
light_purple  = {127, 050, 255, 000, 000}, -- C6170 - Dk Magenta
purple        = {127, 000, 255, 000, 000}, -- C5060 - Dk Blue
light_blue    = {000, 000, 255, 050, 050}, -- C6020 - Lt Lavender
blue          = {000, 000, 255, 000, 000}, -- C5070 - Blue
light_green   = {027, 255, 028, 016, 104}, -- C5081 - VDk Blue2
green         = {049, 255, 055, 120, 090}, -- C4370 - Yel Green
light_cyan    = {060, 230, 109, 000, 245}, -- C4070 - Green
cyan          = {020, 240, 126, 036, 255}, -- C4550 - Turquoise
}

function RobeDmxMap(offset, group, fixture)
  setUniverse(offset + 17, 255) -- RGBW Color mode
  setUniverse(offset +  1, (group.pantilt.x *   255)      )
  setUniverse(offset +  2, (group.pantilt.x * 65535) % 255)
  setUniverse(offset +  3, (group.pantilt.y *   255)      )
  setUniverse(offset +  4, (group.pantilt.y * 65535) % 255)

  setUniverse(offset +  8, (group.red       * 255)        )
  setUniverse(offset +  9, (group.red       * 65535) % 255)
  setUniverse(offset + 10, (group.green     * 255)        )
  setUniverse(offset + 11, (group.green     * 65535) % 255)
  setUniverse(offset + 12, (group.blue      * 255)        )
  setUniverse(offset + 13, (group.blue      * 65535) % 255)
  setUniverse(offset + 14, (group.amber     * 255)        )
  setUniverse(offset + 15, (group.amber     * 65535) % 255)

  local zoom = fixture.zoom
  if zoom == nil then
    zoom = group.zoom
  end
  setUniverse(offset + 18, (zoom      * 255)        )
  setUniverse(offset + 19, (zoom      * 65535) % 255)

  setUniverse(offset + 20, 32) -- shutter open              )
  local intensity = master * group.intensity
  setUniverse(offset + 21, (intensity *   255)      )
  setUniverse(offset + 22, (intensity * 65535) % 255)
  return offset + 22
end


function F415Map(offset, group, fixture) -- 11Ch mode
  local intensity = master * group.intensity
  setUniverse(offset +  1, (intensity *   255)      )
  setUniverse(offset +  2, (intensity * 65535) % 255)

  setUniverse(offset +  3, (group.red       * 255))
  setUniverse(offset +  4, (group.green     * 255))
  setUniverse(offset +  5, (group.blue      * 255))
  setUniverse(offset +  6, (group.amber     * 255))
  setUniverse(offset +  7, (group.lime      * 255))

  setUniverse(offset +  8, 0) -- Strobe
  setUniverse(offset +  9, 0) -- Virtual Color Wheel
  setUniverse(offset + 10, 0) -- Color temperature
  local zoom = fixture.zoom
  if zoom == nil then
    zoom = group.zoom
  end

  setUniverse(offset + 11, (zoom * 255))

  return offset + 11
end

fixture_groups = {
 { -- FRONT
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
 },
 { -- TOP FRONT
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
 },
 { -- TOP BACK
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
 },
 { -- BACK
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
  { translate=F415Map, zoom=1 },
 },
}
--------------------------------------
--    TOUCHOSC CONTROL FUNCTIONS    --
--------------------------------------

pages={}
for i=0,4 do
pages[i] = {}
end

master = 1
ctrl_led = root.children['pager'].children['LED']
ctrl_dim = root.children['pager'].children['dimmer']

function init()

master_fader = root.children['grp_master_fader']:findByName('master_fader')
master = master_fader.values.x

-- Find number of fixture control groups
local f_id = 1
while ctrl_led:findByName(tostring(f_id)) ~= nil do
  f_id = f_id + 1
end
f_id = f_id - 1
print("Found " .. f_id .. " fixture control groups")

-- Find number of dimmer channels
local dim_id = 1
while ctrl_dim:findByName(tostring(dim_id)) ~= nil do
  dim_id = dim_id + 1
end
dim_id = dim_id - 1
print("Found " .. dim_id .. " dimmer control groups")

-- Initialize pages with fixtures and dimmer values

for i=0,table.getn(pages) do
  pages[i] = {
    fixtures = {},
    dimmers = {}
  }
  for j=1,f_id do
    pages[i]['fixtures'][j] = {
      red   = 0.5,
      green = 0.5,
      blue  = 0.5,
      amber = 0.5,
      lime  = 0.5,

      intensity = 0.5,
      pantilt = {x=245/360, y=20/360},

      zoom = 0.5,
      temp_idx = 0, -- Set temperature table
    }
  end

  for j=1,dim_id do
    pages[i]['dimmers'][j] = {
      intensity = 0
    }
  end
end
page_idx = -1
activatePage(0)
end

DMX_RATE = 22 -- Hz
lastDmxUpdate = getMillis()

function update()
  local now = getMillis()
  if (now - lastDmxUpdate > 1000 / DMX_RATE) then
    lastDmxUpdate = now
    translateToDmx()
    transmitDmxMidi(true)
    transmitDmxOsc()
  end

  if (irCmdQueued and now > irCmdSchedule) then
    sendIrCommand(irCmdQueued, true)
    irCmdQueued = nil
  end

end

function activatePage(page_new)
  if(page_idx == page_new) then
    return
  end

  page_idx = page_new

  for i, fg in ipairs(pages[page_idx]['fixtures']) do
    setGroupControls(fg)
  end

  for i, dg in ipairs(pages[page_idx]['dimmers']) do
    setDimmerControls(dg)
  end

  translateToDmx()
  transmitDmxMidi(true)
  transmitDmxOsc()

end

---------------------------
-- LED Fixture specifics --
---------------------------

function setGroupColor(fixtureGroup, color)
  fixtureGroup.red   = color[1] / 255
  fixtureGroup.green = color[2] / 255
  fixtureGroup.blue  = color[3] / 255
  fixtureGroup.amber = color[4] / 255
  fixtureGroup.lime  = color[5] / 255
end

function getFixtureControlGroup(fixture)
  -- Find the group containing the fixture controls
  local group = ctrl_led:findByName(tostring(indexOf(pages[page_idx]['fixtures'], fixture)))
  if(group.type ~= ControlType.GROUP) then
    print("Expected control of type GROUP, got " .. control.type)
    return nil
  end

  return group
end

function setGroupControls(fixtureGroup)
  local controlGroup = getFixtureControlGroup(fixtureGroup)
  -- temperature
  if (fixtureGroup.temp_idx ~= -1) then
    control = controlGroup:findByName('temperature')
    local temp_rgbal = temperatures[fixtureGroup.temp_idx + 1]

    control.values.x = fixtureGroup.temp_idx
    control.color.r = temp_rgbal[1] / 255
    control.color.g = temp_rgbal[2] / 255
    control.color.b = temp_rgbal[3] / 255
  end

  -- intensity
  local control = controlGroup:findByName('intensity')
  control.values.x = fixtureGroup.intensity
  control.color.r = fixtureGroup.red
  control.color.g = fixtureGroup.green
  control.color.b = fixtureGroup.blue

  -- zoom
--  control = controlGroup:findByName('zoom')
--  control.values.x = 1 - fixtureGroup.zoom
--  control.color.r = fixtureGroup.red
--  control.color.g = fixtureGroup.green
--  control.color.b = fixtureGroup.blue
end


------------------------------
-- Dimmer channel specifics --
------------------------------

function getDimmerControlGroup(dimmer)
  -- Find the group containing the fixture controls
  local group = ctrl_dim:findByName(tostring(indexOf(pages[page_idx]['dimmers'], dimmer)))
  if(group.type ~= ControlType.GROUP) then
    print("Expected control of type GROUP, got " .. control.type)
    return nil
  end

  return group
end

function setDimmerControls(dimmer, intensity)
  local controlGroup = getDimmerControlGroup(dimmer)
  local control = controlGroup:findByName('dimmer')
  control.values.x = dimmer.intensity
end

------------------------------
-- UI element callback      --
------------------------------


function onReceiveNotify(msg, ctrl)
  if(msg == "dmxControl") then
    local parameter = ctrl.name


    if(parameter == "master_fader") then
      master = ctrl.values.x

    elseif(parameter == "master_btn") then
      master = ctrl.tag
      master_fader.values.x = master

    elseif(parameter == "dimmer") then
      local dimmer = pages[page_idx]['dimmers'][tonumber(ctrl.parent.name)]
      dimmer.intensity = ctrl.values.x

    else -- LED lamp controls
      local group   = pages[page_idx]['fixtures'][tonumber(ctrl.parent.name)]
      if(parameter == "color") then
        group.temp_idx = -1
        setGroupColor(group, colors[ctrl.tag])
        setGroupControls(group)

      elseif (parameter == "zoom") then
        group.zoom = 1 - ctrl.values.x

      elseif(parameter == "temperature") then
        group.temp_idx = ctrl.values.x
        setGroupControls(group)
        setGroupColor(group, temperatures[group.temp_idx + 1])

      else
        group[parameter] = ctrl.values.x

      end
    end

  elseif(msg == "dmxMemory") then
    activatePage(ctrl.values.x)
  elseif(msg == "irControl") then
    if(ctrl.values.touch and ctrl.values.x ~= 0) then
      sendIrCommand(ctrl.tag)
    end
  end
end


--------------------------------------
--       IR / MIDI FUNCTIONS        --
--------------------------------------

IR_DEVICE_OPTOMA        = 0x32
IR_CMD_OPTOMA_POWER_ON  = 0x02
IR_CMD_OPTOMA_POWER_OFF = 0x2e
IR_CMD_OPTOMA_HDMI2     = 0x9b

function sendIrCommand(cmd)
  print("Sending IR command " .. cmd)
  if(cmd == "projector_on") then
    sendMIDI(bytesToMidi(MIDIMessageType.PITCHBEND, IR_DEVICE_OPTOMA, IR_CMD_OPTOMA_POWER_ON))
  elseif(cmd == "projector_off") then
    sendMIDI(bytesToMidi(MIDIMessageType.PITCHBEND, IR_DEVICE_OPTOMA, IR_CMD_OPTOMA_POWER_OFF))
    irCmdQueued = "projector_off_repeat"
    irCmdSchedule = getMillis() + 1000
  elseif(cmd == "projector_off_repeat") then
    sendMIDI(bytesToMidi(MIDIMessageType.PITCHBEND, IR_DEVICE_OPTOMA, IR_CMD_OPTOMA_POWER_OFF))
  elseif(cmd == "projector_hdmi2") then
    sendMIDI(bytesToMidi(MIDIMessageType.PITCHBEND, IR_DEVICE_OPTOMA, IR_CMD_OPTOMA_HDMI2))
  else
    print("IR command " .. cmd .. " unknown")
  end
end

--------------------------------------
--       OSC / DMX FUNCTIONS        --
--------------------------------------

universe = {}
universe_dirty = {}
for i=1,512 do
  universe[i] = bit32.band(0, 0xff)
  universe_dirty[i] = false
end

function setUniverse(channel, value)
--  print("setting universe " .. channel .. " to " .. value)
--  print("it is now at " .. universe[channel].value)
  if(universe[channel] ~= value) then
    universe_dirty[channel] = true
  end
  universe[channel] = bit32.band(value, 0xff)

end

-- TRANSLATION TO DMX
function translateToDmx()
  local dmxOffset = 0
  for i, dimmer in ipairs(pages[page_idx]['dimmers']) do
    setUniverse(dmxOffset + i, master * dimmer.intensity * 255)
    -- print("Setting dimmer channel " .. i .. " at channel " ..  dmxOffset + i - 1)
  end

  dmxOffset = 99 -- First address of first fixture is one higher (i.e. 100 in this case)
  for i, group in ipairs(pages[page_idx]['fixtures']) do
    for j, fixture in ipairs(fixture_groups[i]) do
      dmxOffset = fixture.translate(dmxOffset, group, fixture)
    end
  end

end

function transmitDmxMidi(all)
  local update = false
  for idx, dirty in ipairs(universe_dirty) do
    if (dirty or all) then
      sendMIDI(bytesToMidi(MIDIMessageType.NOTE_ON, idx-1, universe[idx]))
      universe_dirty[idx] = false
      update = true
    end
  end
--  if update then
    sendMIDI({ MIDIMessageType.NOTE_OFF, 0, 0 })
--  end
end

function transmitDmxOsc()
  sendOSC({'/dmx/set', {{ tag = 'b', value = universe }}})
end