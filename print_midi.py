#!/usr/bin/env python3

import serial

ser = serial.Serial('/dev/ttyUSB0', 1000000)


prev_address = 0
prev_value = 0
testing = True

while True:
    d = ser.read(3)
    address = ((d[0] & 0b1100) << 5) + d[1]
    value   = ((d[0] & 0b0011) << 7) + d[2]
    if testing:
        if prev_address == 511:
            if address != 0:
                print("Expected address cycle")
                print(f"[{d[2]:02x}:{d[1]:02x}:{d[0]:02x}] {address:3d} > {value:3d}")
            else:
                print(f"Cycling for value {value}")
                if value != prev_value + 1:
                    print(f"Expected {prev_value + 1}, got {value}")
                    print(f"[{d[2]:02x}:{d[1]:02x}:{d[0]:02x}] {address:3d} > {value:3d}")
        elif address != prev_address + 1:
            print(f"Expected addr {prev_address + 1}, got {address} (value {value})")
            print(f"[{d[2]:02x}:{d[1]:02x}:{d[0]:02x}] {address:3d} > {value:3d}")

    if(address == 0 and value == 0):
        print("Test started")
        testing = True
    prev_address = address
    prev_value = value
